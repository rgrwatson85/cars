class Customer < User
  has_many :enrollments
  has_many :course_progresses
  has_many :transactions

  #
  #Get the balance owed by a customer by traversing the transactions made
  #
  def balance
    amount = 0.0
    self.transactions.each do |trans|
      amount += trans.amount
    end
    amount
  end

  #
  #Make a payment on a specific transaction
  #
  def make_payment(transaction, amount, payment_method)
    transaction.process_payment(amount, payment_method)
  end
end