class Transaction < ActiveRecord::Base
  belongs_to :customer
  belongs_to :transaction_type
  belongs_to :payment_method
  has_many   :transactions
  belongs_to :transaction
  
  validates_associated :customer
  validates_associated :transaction_type
  validates_associated :payment_method
  validates_associated :transaction

  x = Proc.new{|transaction|
    transaction.amount > 0  || transaction.description.include?('Unenrolled from ')
  }
  validates_presence_of :transaction, :unless => x
  validates_presence_of :payment_method, :unless => x
  
  validates_presence_of :customer
  validates_presence_of :transaction_type
  validates_numericality_of :amount
  validate :amount_less_than_owed, :unless => Proc.new{ |t| t.transaction_id.nil?}
  validates_presence_of :date
  validates :description, :length   => {:minimum => 1, :maximum => 200},
                          :presence => true

  #
  #overwrite amount getters/setters because it is stored in cents not dollars
  #
  def amount
    a = read_attribute(:amount)
    return a.to_f / 100.0 unless !a
    nil
  end

  def amount=(value)
    write_attribute(:amount, value.to_f * 100.0)
  end

  #
  #Makes a child transaction
  #
  def process_payment(amount, payment_type)
    return false if self.transaction_id || self.amount_owed <= 0
    payment = Transaction.new
    payment.customer = self.customer
    payment.amount = amount * -1
    payment.transaction_type = TransactionType.find_by_name('Payment Towards Balance')
    payment.date = Date.today
    payment.description = "Payment towards (#{self.description})"
    payment.payment_method = payment_type
    payment.transaction_id = self.id
    payment.save!
  end

  #
  #Get array of associated transactions
  #
  def child_transactions
    self.transactions
  end

  #
  #Get amount still owed on parent transaction
  #
  def amount_owed
    return self.amount if child_transactions.empty?
    balance = self.amount
    child_transactions.each do |trans|
      balance += trans.amount
    end
    balance
  end

  #
  #Check amount paid is less than amount owed
  #
  def amount_less_than_owed
    parent = Transaction.find(self.transaction_id)
    if parent.amount_owed.abs < self.amount.abs
      errors.add(:amount, " - Must be less than or equal to $#{parent.amount_owed.abs}.")
    end
    if (self.amount * -1) < 0
      errors.add(:amount, " - No refunds allowed, you Jerk!.")
    end
    if self.amount.abs < 1
      errors.add(:amount, " - Must be $1.00 and $#{parent.amount_owed.abs}.")
    end
  end

end
