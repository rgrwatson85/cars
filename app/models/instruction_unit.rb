class InstructionUnit < ActiveRecord::Base
  belongs_to :course
  has_many :course_progresses
  
  validates_presence_of :name
  validates_presence_of :sequence
  validates_presence_of :course
  validates_associated  :course
  validates_uniqueness_of :sequence, :scope => :course
end
