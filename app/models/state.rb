class State < ActiveRecord::Base
  belongs_to :country
  has_many :users
  has_many :locations
  
  validates :name, :presence => true, :uniqueness => true
  validates_presence_of :country
  validates_associated :country
end
