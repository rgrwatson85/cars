class TransactionType < ActiveRecord::Base
  has_many :transactions
  validates :name, :presence => true, :uniqueness => true
  validates_numericality_of :due_in_days, :greater_than_or_equal_to => 0
end
