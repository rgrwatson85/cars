class CourseProgress < ActiveRecord::Base
  belongs_to :customer
  belongs_to :instruction_unit
  belongs_to :section

  validates_presence_of     :customer, :instruction_unit, :section
  validates_uniqueness_of   :instruction_unit_id, :scope => [:customer_id, :section_id]

end
