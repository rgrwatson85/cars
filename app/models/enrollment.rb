class Enrollment < ActiveRecord::Base
  belongs_to :customer
  belongs_to :section
  has_many :attendances
  belongs_to :transaction

  validates_presence_of :customer
  validates_presence_of :section
  validates_uniqueness_of :customer, :scope => :section, :message => 'already enrolled in this section'
  validate :prerequisites_met?

  after_create :log_enrollment_transaction
  after_destroy :log_unenrollment_transaction


  def prerequisites_met?
    course           = section.course
    customer_courses = []

    #get a list of all classes customer has taken
    customer.enrollments.each do |enrollment|
      customer_courses.push(enrollment.section.course)
    end

    #get a list of prerequisites and check if customer has taken them
    course.prerequisites(true).each do |prereq|
      rule = CoursePrerequisite.where(:course => course, :prerequisite => prereq).take
      if rule && !rule.allow_concurrent?
        if !customer_courses.include? prereq
          errors.add(:section, "Prerequisite Not Taken! (#{prereq.name})")
        else
          units_taken = []
          #iterate through all instruction units taken by customer
          customer.course_progresses.each do |unit|
            if unit.section.course == prereq
              #the instruction unit is part of the prereq course
              units_taken.push(unit)
            end
          end

          #check all prereq instruction units have been completed
          prereq.instruction_units.all.each do |piu|
            if !units_taken.include? piu
              errors.add(:section, "Prerequisite Not Complete! (#{prereq.name})")
              break
            end
          end

        end
      #check that the customer is currently enrolled in the prerequisite if concurreny allowed
      elsif rule && rule.allow_concurrent?
        if !customer_courses.include? prereq
          errors.add(:section, "Must enroll in #{prereq.name} first!")
        end
      end

      course = prereq
    end
  end

  def log_enrollment_transaction
    t                  = Transaction.new
    t.customer         = customer
    t.transaction_type = TransactionType.find_by_name('Enroll In Course')
    t.amount           = section.course.cost
    t.date             = Date.today
    t.description      = "Enrolled in #{section.course.name}"
    t.payment_method   = nil
    t.transaction_id   = nil
    t.save!
    self.update(:transaction => t)
  end

  def log_unenrollment_transaction
    t                  = Transaction.new
    t.customer         = customer
    t.transaction_type = TransactionType.find_by_name('Unenroll From Course')
    t.amount           = calculate_refund_amount
    t.date             = Date.today
    if t.amount == 0
      t.description    = "Unenrolled from #{section.course.name} - No Refund"
    else
      t.description    = "Unenrolled from #{section.course.name}"
    end
    t.payment_method   = nil
    t.transaction      = transaction
    t.save!
  end

  def calculate_refund_amount
    start_date  = section.start_time
    gets_refund = Date.today - 5.days < start_date
    gets_refund ? transaction.amount_owed * -1 : 0
  end
end