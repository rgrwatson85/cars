class Section < ActiveRecord::Base
  include IceCube
  serialize :schedule, Hash

  belongs_to :course
  belongs_to :employee
  belongs_to :location
  has_many :enrollments
  has_many :course_progresses

  # A note about how this model handles its schedule object:
  # The object is assumed at all times to only have one rule. That rule is also assumed
  # to be a weekly rule and to have only one day validation and no others.
  #
  # When directly writing this model's schedule, whether internally or externally, it
  # is important to only write the full, complete schedule object with only the one rule.
  # This is especially important when creating the object originally.

  # Defines a range of dates in which a section is considered enrollable. Used to determine which to show in some views.
  CURRENT_WINDOW = (Date.today - 2.days)..(Date.today + 6.weeks)

  # Defines a list of symbols representing days of the week. Used to map some values to weekdays.
  DAYS_OF_WEEK = [:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday]

  validates_presence_of :course, :free_seats, :employee, :location, :schedule
  validates_associated :course, :employee, :location
  validates_numericality_of :free_seats,
                            only_integer: true,
                            greater_than_or_equal_to: 0,
                            message: 'can only be a positive, whole number.'
  validate :does_not_conflict, :only_one_weekly_rule, :is_finite, :classes_terminate, :has_positive_duration

  #
  # VIRTUAL ATTRIBUTES
  #

  # Accepts day name symbols to set the section schedule recurrence, either as enumerable or direct arguments
  def recurs_on=(days)
    temp_schedule = self.schedule
    unless temp_schedule.recurrence_rules.count == 0
      temp_schedule.remove_recurrence_rule(temp_schedule.recurrence_rules.first)
    end
    temp_schedule.add_recurrence_rule(Rule.weekly.day(*days).count(self.course.instruction_units.count))
    self.schedule = temp_schedule
  end

  # Returns array of day name symbols for the recurrence
  def recurs_on
    rules = self.schedule.recurrence_rules.first.validations_for(:day)
    rules.map! { |validation| validation.day }
    rules.map { |index| DAYS_OF_WEEK[index] }
  end

  # Sets the beginning `Time` of the first class meeting time
  def start_time=(time)
    temp_schedule = self.schedule
    temp_schedule.start_time = time
    self.schedule = temp_schedule
  end

  # Returns the `Time` of the first class meeting time
  def start_time
    self.schedule.start_time
  end

  # Sets the end `Time` of the first occurrence of the section
  def end_time=(time)
    temp_schedule = self.schedule
    temp_schedule.end_time = time
    self.schedule = temp_schedule
  end

  # Returns the end `Time` of the first class meeting time
  def end_time
    self.schedule.end_time
  end

  # Returns the duration of one occurrence of this section in seconds
  def duration
    self.schedule.duration
  end

  # Sets the duration of an occurrence of the section
  def duration=(seconds)
    temp_schedule = self.schedule
    temp_schedule.duration = seconds
    self.schedule = temp_schedule
  end

  # Returns the last start `Time` of the section's occurrences
  def last_start_time
    self.schedule.last
  end

  # Returns the last end `Time` of the section's occurrences
  def last_end_time
    self.schedule.last + self.schedule.duration
  end

  #
  # ATTRIBUTE ACCESSOR OVERRIDES
  #

  # Set schedule object directly
  def schedule=(new_schedule)
    write_attribute(:schedule, new_schedule.to_hash)
  end

  # Read schedule object directly
  def schedule
    Schedule.from_hash(read_attribute(:schedule))
  end

  #
  # CONVENIENCE METHODS
  #

  # Returns true if the section starts within the current enrollment period
  def current?
    true if CURRENT_WINDOW.include?(self.schedule.start_time.to_date)
  end

  # Returns an abbreviated string representing the recurrence rule (ex. "Mon-Wed-Fri")
  def recurrence_string
    self.schedule
    .recurrence_rules
    .first
    .validations_for(:day).map do |validation|
      validation.day
    end.map do |day_index|
      Date::ABBR_DAYNAMES[day_index]
    end.join('-')
    # Gnarly!
  end

  private

  #
  # VALIDATION CALLBACKS
  #

  # Confirms that this schedule does not conflict with another location or employee
  def does_not_conflict
    # lol...
    # http://www.downloadmoreram.com/
    Section.all.each do |section|
      if  (section.location == self.location) and
          (section.employee == self.employee) and
          (section.schedule.conflicts_with?(self.schedule))
        errors.add(:schedule, 'conflicts with another.')
        break
      end
    end
  end

  # Confirms that this schedule has only one rule and that it repeats weekly
  def only_one_weekly_rule
    if (self.schedule.recurrence_rules.length == 1) and not
        (self.schedule.recurrence_rules.first.kind_of?(IceCube::WeeklyRule))
      errors.add(:schedule, 'has an invalid number of rules on it or they aren\'t weekly.')
    end
  end

  # Confirms that this schedule terminates
  def classes_terminate
    unless self.schedule.terminating?
      errors.add(:schedule, 'does not terminate. Can\'t have infinitely recurring sections.')
    end
  end

  # Confirms that this section has a finite duration.
  def is_finite
    if self.duration == 0
      errors.add(:schedule, 'has no end date.')
    end
  end

  # Confirms that the schedule's start time occurs before the end time
  def has_positive_duration
    if self.schedule.duration < 0
      errors.add(:schedule, 'has an end date before it\'s start date')
    end
  end
end
