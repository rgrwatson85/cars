class Location < ActiveRecord::Base
  belongs_to :state
  has_many :sections

  validates_presence_of :address, :city, :state, :postal_code

end
