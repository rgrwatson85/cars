class CoursePrerequisite < ActiveRecord::Base
  belongs_to :course
  belongs_to :prerequisite, :class_name => 'Course'

  validates_associated :course
  validates_associated :prerequisite
  validates_inclusion_of :allow_concurrent, :in => [true, false]
  validates_uniqueness_of :prerequisite, :scope => :course
  
  def allow_concurrent?
    self.allow_concurrent
  end
end
