class Attendance < ActiveRecord::Base
  belongs_to :attendance_type
  belongs_to :enrollment

  validates_presence_of :attendance_type
  validates_presence_of :enrollment
  validates_presence_of :record_date
  validates_uniqueness_of :enrollment_id, :scope => :record_date, :message => 'already taken for this date'
  validate :record_date_not_in_future

  private

  def record_date_not_in_future
    errors.add(:record_date, 'cannot be set for a future date') unless record_date <= Date.today
  end

end
