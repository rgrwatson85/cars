class Course < ActiveRecord::Base  
  after_initialize :set_defaults
  has_many :course_prerequisites
  has_many :courses, through: :course_prerequisites
  has_many :sections
  has_many :instruction_units
  
  DEFAULT_NAME          = 'Default Course Name'
  DEFAULT_COST          = 0
  DEFAULT_FLOATING_DATE = false
  
  validates_presence_of     :name, :cost
  validates_inclusion_of    :floating_date, :in => [true, false]
  validates_uniqueness_of   :name
  validates_exclusion_of    :name, :in => [DEFAULT_NAME]
  validates_numericality_of :cost, :greater_than_or_equal_to => 0
  
  #
  #overwrite cost getters/setters because it is stored in cents not dollars
  #
  def cost
    c = read_attribute(:cost)
    return c.to_f / 100.0 unless !c
    nil     
  end
  
  def cost=(value)
    write_attribute(:cost, value.to_f * 100.0)
  end
  
  #
  #give prerequisites for this course.
  #
  #  args[0] : Do you want to do a recursive search of prerequisites?
  #  args[1] : Flag set when method called recursively - Do not set manually!
  #
  def prerequisites(*args)
    if !args || !args[0]
      prerequisites = []
      CoursePrerequisite.where(:course_id => self.id).each do |prerequisite|
        prerequisites.push(prerequisite.prerequisite)
      end
      return prerequisites 
    elsif args && args[0]
      sublevel_flag = args[1] || false
      if self.has_prerequisites?
        self.prerequisites.each do |prerequisite|
          #recursive check
          if prerequisite.has_prerequisites?
            prerequisite.prerequisites(true, true)
          end
          self.class.temp.push(prerequisite)
        end
      end
      if !sublevel_flag
        prerequisites   = self.class.temp.clone
        self.class.temp = []
        prerequisites.reverse
      end
    end
  end
  
  #
  #check if a course has any prerequisites.
  #
  def has_prerequisites?
    self.prerequisites.length >= 1
  end
  
  #
  #give all courses that have this course as a prerequisite.
  #
  #  args[0] : Do you want to do a recursive search of courses that require this course?
  #  args[1] : Flag set when method called recursively - Do not set manually!
  #
  def required_by(*args)
    if !args || !args[0]
      courses = []
      CoursePrerequisite.where(:prerequisite_id => self.id).each do |course|
        courses.push(course.course)
      end
      return courses
    elsif args && args[0]
      sublevel_flag = args[1] || false
      self.required_by.each do |course|
        if course.is_required?
          course.required_by(true, true)
        end
        self.class.temp.push(course)
      end
      if !sublevel_flag
        courses         = self.class.temp.clone
        self.class.temp = []
        courses
      end
    end
  end
  
  #
  #check if a course is a prerequisite of another.
  #
  def is_required?
    self.required_by.length >= 1
  end
  
  #
  #check if two courses can be taken concurrently.
  #
  def concurrent_enrollment_allowed?(prerequisite)
    
    #check if the prerequisite given is a direct prerequisite of this course.
    #if it is then a single check for allow concurrent can be done.
    m = CoursePrerequisite.where(:prerequisite_id => prerequisite.id, 
                                 :course_id       => self.id).take
    return false if m.is_a?(CoursePrerequisite) && !m.allow_concurrent?
    
    prerequisites = self.prerequisites(true)#use recursive flag to traverse entire tree.
    required_by   = prerequisite.required_by(true)#use recursive flag to traverse entire tree.
    
    #search for matching courses in prerequisites and required_by.
    #if there are no matches then there no dependencies, so we allow concurrent enrollment.
    matches = prerequisites & required_by
    return true unless !matches.empty?
    
    #check all matches for a prerequisites that doesn't allow concurrent enrollement.
    #if any do not allow concurrent enrollment, then there is a block on the
    #prerequisite tree between the two courses.
    matches.each do |match|
      match.required_by.each do |required_by|
        rule = CoursePrerequisite.where(:prerequisite_id => match.id, 
                                        :course_id       => required_by.id).take
        return false unless rule.allow_concurrent?                      
      end
    end
    true
  end

  def self.enrollable
    self.includes(:sections).where('sections.start_time' => Section::CURRENT_WINDOW).references(:sections)
  end

  def self.open_seats
    self.includes(:sections).where('sections.free_seats > ?', 0).references(:sections)
  end

  private
  
  #
  #array used for storing values while recursively searching.
  #
  @@temp = []
  def self.temp
    @@temp
  end
  def self.temp=(val)
    @@temp = val
  end
  
  #
  #set defaults for new object.
  #
  def set_defaults
    self.name          ||= DEFAULT_NAME
    self.cost          ||= DEFAULT_COST
    self.floating_date ||= DEFAULT_FLOATING_DATE
  end
  
end
