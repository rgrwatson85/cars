class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :validatable
  belongs_to :last_name_suffix
  belongs_to :state
  belongs_to :salutation


  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :home_phone, :presence => true, format: { with: /\A\d{3}-\d{3}-\d{4}\z/, message: "- bad format. Please use: 123-456-7890" }
  validates :cell_phone, :presence => true, format: { with: /\A\d{3}-\d{3}-\d{4}\z/, message: "- bad format. Please use: 123-456-7890" }
  validates :address, :presence => true
  validates :city, :presence => true
  validates :state, :presence => true
  validates :postal_code, :presence => true, format: { with: /\A\d{5}(-\d{4})?\z/, message: "- bad format. Please use: 12345 or 12345-6789" }
  validates :dob, :presence => true
  validates :salutation, :presence => true

  validates :type, :presence => true
  validates_inclusion_of :type, :in => %w( Customer Employee )

  #Method that returns if the logged in user is an employee.
  def is_employee?
    self.type == 'Employee' ? true : false
  end

  #Method that returns if the logged in user is a customer.
  def is_customer?
    self.type == 'Customer' ? true : false
  end

  def full_name
    self.salutation.name + ' ' \
    + self.first_name + ' ' \
    + self.last_name \
    + (self.last_name_suffix.nil? ? '' : ', ' + self.last_name_suffix.name)
  end

  def full_address
    self.address + '<br/>' \
    + (self.address2.nil? ? '' : ' ' + self.address2) \
    + self.city + ' ' + self.state.abbr + ', ' + self.postal_code
  end

end
