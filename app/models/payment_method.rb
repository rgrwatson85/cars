class PaymentMethod < ActiveRecord::Base
  has_many :transactions
  validates :name, :presence => true, :uniqueness => true
end
