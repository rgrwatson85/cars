$('document').ready(function(){

    function set_form(form_h, form_s){
        form_s.show();
        if (form_h){ form_h.hide()};
        bind_buttons();
    }

    function bind_buttons(){
        $('#registration_form1_next').on('click', function(e){
            e.preventDefault();
            set_form($('#registration_form1'),$('#registration_form2'));
        });

        $('#registration_form2_previous').on('click', function(e){
            e.preventDefault();
            set_form($('#registration_form2'),$('#registration_form1'));
        });
        $('#registration_form2_next').on('click', function(e){
            e.preventDefault();
            set_form($('#registration_form2'),$('#registration_form3'));
        });

        $('#registration_form3_previous').on('click', function(e){
            e.preventDefault();
            set_form($('#registration_form3'),$('#registration_form2'));
        });
        $('#registration_form3_next').on('click', function(e){
            e.preventDefault();
            set_form($('#registration_form3'),$('#registration_form4'));
        });

        $('#registration_form4_previous').on('click', function(e){
            e.preventDefault();
            set_form($('#registration_form4'),$('#registration_form3'));
        });
    }

    set_form(null, $('#registration_form1'));
    $('#registration_form2').hide();
    $('#registration_form3').hide();
    $('#registration_form4').hide();

    $('#user_dob').datepicker({dateFormat: 'yy-mm-dd'})
});