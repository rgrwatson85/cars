class EnrollmentsController < ApplicationController
  before_action :set_enrollment, only: [:show, :edit, :update, :destroy]
  before_filter do
    @customer = nil
    if params[:customer_id]
      @customer = Customer.find(params[:customer_id])
    end
    if params[:section_id]
      @section = Section.find(params[:section_id])
    end
  end

  # GET /enrollments
  # GET /enrollments.json
  def index
    if params[:customer_id]
      @enrollments = Enrollment.where(:customer_id => @customer.id)
    elsif params[:section_id]
      @enrollments = Enrollment.where(:section_id => @section.id)
    else
      @enrollments = Enrollment.all
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /enrollments/1
  # GET /enrollments/1.json
  def show
  end

  # GET /enrollments/new
  def new
    @enrollment = Enrollment.new
  end

  # GET /enrollments/1/edit
  def edit
  end

  # POST /enrollments
  # POST /enrollments.json
  def create
    @enrollment = Enrollment.new(enrollment_params)
    @enrollment.customer = @customer

    respond_to do |format|
      if @enrollment.save
        format.html { redirect_to customer_enrollment_path(@customer), notice: 'Enrollment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @enrollment }
        format.js
      else
        format.html { render action: 'new' }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
        format.js   { render action: 'new'}
      end
    end
  end

  # PATCH/PUT /enrollments/1
  # PATCH/PUT /enrollments/1.json
  def update
    respond_to do |format|
      if @enrollment.update(enrollment_params)
        format.html { redirect_to @enrollment, notice: 'Enrollment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enrollments/1
  # DELETE /enrollments/1.json
  def destroy
    @enrollment.destroy
    respond_to do |format|
      format.html { redirect_to enrollments_url }
      format.json { head :no_content }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enrollment
      @enrollment = Enrollment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enrollment_params
      params.require(:enrollment).permit(:customer_id, :section_id)
    end
end
