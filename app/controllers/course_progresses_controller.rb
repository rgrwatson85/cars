class CourseProgressesController < ApplicationController
  before_action :set_course_progress, only: [:show, :edit, :update, :destroy]
  before_filter do
    @enrollment = Enrollment.find(params[:enrollment_id])
  end

  # GET /course_progresses
  # GET /course_progresses.json
  def index
    @not_taken        = InstructionUnit.where(:course => @enrollment.section.course).all
    @completed_units  = CourseProgress.where(:section => @enrollment.section, :customer => @enrollment.customer).all
    @completed_units.each do |cp|
      @not_taken.delete_if {|nt| nt == cp.instruction_unit}
    end
  end

  # GET /course_progresses/1
  # GET /course_progresses/1.json
  def show
  end

  # GET /course_progresses/new
  def new
    @course_progress = CourseProgress.new
  end

  # GET /course_progresses/1/edit
  def edit
  end

  # POST /course_progresses
  # POST /course_progresses.json
  def create
    @course_progress = CourseProgress.new(course_progress_params)

    respond_to do |format|
      if @course_progress.save
        format.html { redirect_to @course_progress, notice: 'Course progress was successfully created.' }
        format.json { render action: 'show', status: :created, location: @course_progress }
      else
        format.html { render action: 'new' }
        format.json { render json: @course_progress.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /course_progresses/1
  # PATCH/PUT /course_progresses/1.json
  def update
    respond_to do |format|
      if @course_progress.update(course_progress_params)
        format.html { redirect_to @course_progress, notice: 'Course progress was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @course_progress.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /course_progresses/1
  # DELETE /course_progresses/1.json
  def destroy
    @course_progress.destroy
    respond_to do |format|
      format.html { redirect_to course_progresses_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course_progress
      @course_progress = CourseProgress.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_progress_params
      params[:course_progress]
    end
end
