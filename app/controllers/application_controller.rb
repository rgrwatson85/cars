class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Used for allowing devise to modify the database.
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :authenticate_user!
  before_filter :redirect_customer, :unless => :devise_controller?
  before_filter :block_ie

  protected

  # Specify parameters that the sign_up page passes to the database.
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :password_confirmation, :first_name, :last_name, :last_name_suffix_id, :home_phone, :cell_phone, :address, :address2, :city,:state_id, :postal_code, :dob, :salutation_id, :type)}
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:email, :password, :password_confirmation, :current_password, :first_name, :last_name, :last_name_suffix_id, :home_phone, :cell_phone, :address, :address2, :city,:state_id, :postal_code, :dob, :salutation_id, :type)}
  end

  #redirect customer to their profile if they try to access non-allowed resources
  def redirect_customer
    if %w(users).include?(params[:controller]) && %w(index).include?(params[:action])
      if current_user && current_user.is_customer?
        redirect_to customer_path(current_user), :error => 'You are not allowed to view this resource.'
      end
    end
  end

  # Specifically block Internet Explorer
  def block_ie
    if browser.ie?
      render :status => "418", text: "I am a teapot and Internet Explorer is not supported. Click <a href='http://firefox.com'>here</a> to download Mozilla Firefox<a href='http://en.wikipedia.org/wiki/Hyper_Text_Coffee_Pot_Control_Protocol'>.</a>"
    end
  end
end