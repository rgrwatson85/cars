class InstructionUnitsController < ApplicationController
  before_action :set_instruction_unit, only: [:show, :edit, :update, :destroy]

  # GET /instruction_units
  # GET /instruction_units.json
  def index
    @instruction_units = InstructionUnit.all
  end

  # GET /instruction_units/1
  # GET /instruction_units/1.json
  def show
  end

  # GET /instruction_units/new
  def new
    @instruction_unit = InstructionUnit.new
  end

  # GET /instruction_units/1/edit
  def edit
  end

  # POST /instruction_units
  # POST /instruction_units.json
  def create
    @instruction_unit = InstructionUnit.new(instruction_unit_params)

    respond_to do |format|
      if @instruction_unit.save
        format.html { redirect_to @instruction_unit, notice: 'Instruction unit was successfully created.' }
        format.json { render action: 'show', status: :created, location: @instruction_unit }
      else
        format.html { render action: 'new' }
        format.json { render json: @instruction_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /instruction_units/1
  # PATCH/PUT /instruction_units/1.json
  def update
    respond_to do |format|
      if @instruction_unit.update(instruction_unit_params)
        format.html { redirect_to @instruction_unit, notice: 'Instruction unit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @instruction_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /instruction_units/1
  # DELETE /instruction_units/1.json
  def destroy
    @instruction_unit.destroy
    respond_to do |format|
      format.html { redirect_to instruction_units_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_instruction_unit
      @instruction_unit = InstructionUnit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def instruction_unit_params
      params[:instruction_unit]
    end
end
