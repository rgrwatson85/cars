class AttendancesController < ApplicationController
  before_action :set_attendance, only: [:show, :edit, :update, :destroy]
  before_filter do
    @section     = nil
    @enrollments = nil
    @customers   = nil

    if params[:section_id]
      @section     = Section.find(params[:section_id])
      unless params[:enrollment_id]
        @enrollments = Enrollment.where(:section => @section)
        @customers   = []
        @enrollments.each do |e|
          @customers.push(e.customer)
        end
      else
        @enrollment = Enrollment.find(params[:enrollment_id])
      end
      @instruction_unit = InstructionUnit.where(:course => @section.course).take
    end
  end

  # GET /attendances
  # GET /attendances.json
  def index
    if params[:section_id]
      unless params[:enrollment_id]
        @attendances = Attendance.where(:enrollment_id => @enrollments).order(:record_date => :desc)
      else
        @attendances = Attendance.where(:enrollment_id => params[:enrollment_id]).order(:record_date => :desc)
      end
    else
      @attendances = Attendance.all.order(:record_date => :desc)
    end
  end

  # GET /attendances/1
  # GET /attendances/1.json
  def show
  end

  # GET /attendances/new
  def new
    @attendance = Attendance.new

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /attendances/1/edit
  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /attendances
  # POST /attendances.json
  def create
    @attendance             = Attendance.new(attendance_params)
    @attendance.record_date = Date.today
    respond_to do |format|
      if @attendance.save
        format.html { redirect_to [@section, @attendance], notice: 'Attendance was successfully created.' }
        format.json { render action: 'show', status: :created, location:[@section,@attendance] }
      else
        format.html { render action: 'new' }
        format.json { render json: @attendance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /attendances/1
  # PATCH/PUT /attendances/1.json
  def update
    respond_to do |format|

      attendance_type_id = params['attendance']['1']['attendance_type_id']
      @attendance.attendance_type_id = attendance_type_id

      if @attendance.save
        cp_params = {:customer            => @attendance.enrollment.customer,
                     :section             => @section,
                     :instruction_unit_id => params[:instruction_unit]['id'].to_i}

        cp = CourseProgress.where(cp_params).first

        unless cp
          cp = CourseProgress.new(cp_params)
          cp.created_at = @attendance.created_at
        end

        ['Present', 'Late For Class'].include?(@attendance.attendance_type.name) ? cp.save : cp.destroy

        format.html { redirect_to [@section, @attendance], notice: 'Attendance was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: 'edit' }
        format.json { render json: @attendance.errors, status: :unprocessable_entity }
        format.js   { render :js => 'alert("Attendance Update Failed!")'}
      end
    end
  end

  # DELETE /attendances/1
  # DELETE /attendances/1.json
  def destroy
    @attendance.destroy
    respond_to do |format|
      format.html { redirect_to [@section, @attendances] }
      format.json { head :no_content }
    end
  end

  def take

    respond_to do |format|
      format.html {
        params[:attendance].each do |attendance|
          next if attendance[0] == 'record_date'
          att             = Attendance.new(attendance[1])
          att.record_date = Date.parse(params[:attendance]['record_date'])
          search          = Attendance.where(:enrollment_id => att.enrollment_id, :record_date => att.record_date).take
          if search
            search.attendance_type_id = att.attendance_type_id
            unless search.save
              render new_section_attendance_path(@section)
            end
          else
            unless att.save
              render new_section_attendance_path(@section)
            end
          end
        end
        redirect_to section_attendances_path(@section), notice: 'Attendance was successfully taken.'
      }

      format.js {
        params[:attendance].each do |attendance|
          next if attendance[0] == 'record_date'
          att             = Attendance.new(attendance[1])
          att.record_date = Date.parse(params[:attendance]['record_date'])
          search          = Attendance.where(:enrollment_id => att.enrollment_id, :record_date => att.record_date).take
          if search
            search.attendance_type_id = att.attendance_type_id
            unless search.save
              render :action => 'new'
            end
          else
            unless att.save
              render :action => 'new'
            end
          end
          #create a course progress for attendance
          cp = CourseProgress.where(:customer            => att.enrollment.customer,
                                    :section             => @section,
                                    :instruction_unit_id => params[:instruction_unit]['id'].to_i).first_or_create

          valid = AttendanceType.where(:name => ['Present', 'Late For Class'])

          valid.include?(att.attendance_type) ? cp.save : cp.destroy
        end
      }
    end

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_attendance
    @attendance = Attendance.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def attendance_params
    params.require(:attendance).permit(:attendance_type_id, :enrollment_id)
  end
end
