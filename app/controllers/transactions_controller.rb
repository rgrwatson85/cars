class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_filter do
    @customer = Customer.find(params[:customer_id])
    @parent_transaction = nil
    if params[:transaction_id]
      @parent_transaction = Transaction.find(params[:transaction_id])
    end
  end

  # GET /transactions
  # GET /transactions.json
  def index
    @transactions = Transaction.all.where(:customer_id => @customer.id)
    respond_to do |format|
      format.html {redirect_to :root, :notice => 'Transactions must be viewed from your profile.'}
      format.js
    end
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
    respond_to do |format|
      format.html {redirect_to :root, :notice => 'Transactions must be viewed from your profile.'}
      format.js
    end
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
    @transaction.transaction_id = @parent_transaction
    respond_to do |format|
      format.html{redirect_to :root, :notice => 'Transactions must be created from your profile.'}
      format.js
    end
  end

  # GET /transactions/1/edit
  def edit
    respond_to do |format|
      format.html{redirect_to :root, :notice => 'Transactions cannot be edited.'}
      format.js
    end
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.customer = @customer
    @transaction.transaction = @parent_transaction
    @transaction.amount *= -1
    @transaction.transaction_type = TransactionType.find_by_name('Payment Towards Balance')
    @transaction.date = Date.today

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to :back, notice: 'Transaction was successfully created.' }
        format.json { render action: 'show', status: :created, location: customer_transaction_path(@customer, @transaction) }
        format.js
      else
        format.html { render action: 'new' }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
        format.js   { render :action => 'new'}
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transaction was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:transaction_type_id, :amount, :date, :description, :payment_method_id, :transaction_id, :customer_id)
    end
end
