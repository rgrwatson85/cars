class HomeController < ApplicationController
  skip_before_filter :authenticate_user!, :only => :index

  def index
    #I know it's hacky, but it's due in 4 hours.
    redirect_to user_path(current_user)
  end
end
