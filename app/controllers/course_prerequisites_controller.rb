class CoursePrerequisitesController < ApplicationController
  before_action :set_course_prerequisite, only: [:show, :edit, :update, :destroy]
  before_filter do
    @course = Course.find(params[:course_id])
  end

  # GET /course_prerequisites
  # GET /course_prerequisites.json
  def index
    @course_prerequisites = CoursePrerequisite.all
  end

  # GET /course_prerequisites/1
  # GET /course_prerequisites/1.json
  def show
  end

  # GET /course_prerequisites/new
  def new
    @course_prerequisite = CoursePrerequisite.new
  end

  # GET /course_prerequisites/1/edit
  def edit
  end

  # POST /course_prerequisites
  # POST /course_prerequisites.json
  def create
    @course_prerequisite = CoursePrerequisite.new(course_prerequisite_params)

    respond_to do |format|
      if @course_prerequisite.save
        format.html { redirect_to [@course, @course_prerequisite], notice: 'Course prerequisite was successfully created.' }
        format.json { render action: 'show', status: :created, location: [@course,@course_prerequisite] }
      else
        format.html { render action: 'new' }
        format.json { render json: @course_prerequisite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /course_prerequisites/1
  # PATCH/PUT /course_prerequisites/1.json
  def update
    respond_to do |format|
      if @course_prerequisite.update(course_prerequisite_params)
        format.html { redirect_to [@course,@course_prerequisite], notice: 'Course prerequisite was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @course_prerequisite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /course_prerequisites/1
  # DELETE /course_prerequisites/1.json
  def destroy
    @course_prerequisite.destroy
    respond_to do |format|
      format.html { redirect_to course_course_prerequisites_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course_prerequisite
      @course_prerequisite = CoursePrerequisite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_prerequisite_params
      params.require(:course_prerequisite).permit(:allow_concurrent)
    end
end
