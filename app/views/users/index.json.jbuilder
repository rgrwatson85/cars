json.array!(@users) do |user|
  json.extract! user, :first_name, :last_name, :last_name_suffix_id, :home_phone, :cell_phone, :address, :address2, :city, :state_id, :postal_code, :dob, :salutation_id
  json.url user_url(user, format: :json)
end
