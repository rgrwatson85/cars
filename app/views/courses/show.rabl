object @course

attribute :name, :cost, :location


child :sections do
    attribute :start_time, :end_time, :free_seats
    child :employee do
        attribute :first_name, :last_name
    end
    child :location do
        attribute :address, :address2, :city, :postal_code
        child :state do
            attribute :name
        end
    end
end

