json.array!(@instruction_units) do |instruction_unit|
  json.extract! instruction_unit, 
  json.url instruction_unit_url(instruction_unit, format: :json)
end
