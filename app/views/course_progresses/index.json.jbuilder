json.array!(@course_progresses) do |course_progress|
  json.extract! course_progress, 
  json.url course_progress_url(course_progress, format: :json)
end
