json.array!(@transactions) do |transaction|
  json.extract! transaction, :transaction_type_id, :amount, :date, :description, :payment_method_id, :transaction_id, :customer_id
  json.url transaction_url(transaction, format: :json)
end
