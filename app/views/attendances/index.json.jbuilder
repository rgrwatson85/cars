json.array!(@attendances) do |attendance|
  json.extract! attendance, 
  json.url attendance_url(attendance, format: :json)
end
