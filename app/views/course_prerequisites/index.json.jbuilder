json.array!(@course_prerequisites) do |course_prerequisite|
  json.extract! course_prerequisite, 
  json.url course_prerequisite_url(course_prerequisite, format: :json)
end
