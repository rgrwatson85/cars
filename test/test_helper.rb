ENV["RAILS_ENV"] ||= "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'turn/autorun'

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  #fixtures :all

  # Load some select seed data for testing
  # Additional data required by tests should be loaded in the test itself, either
  # from the seeds or via fixtures

  # Ordered lookup tables
  load "#{Rails.root}/db/seeds_data/countries.rb"
  load "#{Rails.root}/db/seeds_data/states.rb"

  # Unordered lookup tables
  load "#{Rails.root}/db/seeds_data/attendance_types.rb"
  load "#{Rails.root}/db/seeds_data/courses.rb"
  load "#{Rails.root}/db/seeds_data/course_prerequisites.rb"
  load "#{Rails.root}/db/seeds_data/instruction_units.rb"
  load "#{Rails.root}/db/seeds_data/last_name_suffixes.rb"
  load "#{Rails.root}/db/seeds_data/locations.rb"
  load "#{Rails.root}/db/seeds_data/payment_methods.rb"
  load "#{Rails.root}/db/seeds_data/salutations.rb"
  load "#{Rails.root}/db/seeds_data/transaction_types.rb"

  # Turn gem configuration
  Turn.config do |c|
    c.format  = :pretty
    c.trace   = 1
    c.natural = true
    c.ansi    = true
  end

end
