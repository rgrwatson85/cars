require 'test_helper'

class LastNameSuffixTest < ActiveSupport::TestCase
  
  setup do
    @lns = LastNameSuffix.new
  end
  
  test 'name must be present and unique' do
    assert !@lns.save
    @lns.name = 'Jr.'
    assert @lns.save
    @lns.save
    @lns      = LastNameSuffix.new
    @lns.name = 'Jr.'
    assert !@lns.save
  end

end
