require 'test_helper'

class EnrollmentTest < ActiveSupport::TestCase

  setup do
    @enrollment = Enrollment.new
  end

  test 'a customer cannot be double enrolled in a course' do
    @enrollment.customer_id = 1
    @enrollment.section_id = 1
    assert @enrollment.save
    @enrollment = Enrollment.new
    @enrollment.customer_id = 1
    @enrollment.section_id = 1
    refute @enrollment.save
  end



end

