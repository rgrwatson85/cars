require 'test_helper'

class StateTest < ActiveSupport::TestCase

  setup do
    @state = State.new
  end
  
  test 'name must be present and unique and country id is present' do
    assert !@state.save
    @state.name = 'test'
    assert !@state.save
    @state.country_id = 1
    assert @state.save
    @state.save
    @state      = State.new
    @state.name = 'test'
    assert !@state.save
  end

end
