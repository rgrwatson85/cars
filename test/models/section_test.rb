require 'test_helper'

class SectionTest < ActiveSupport::TestCase
  setup do
    @section = Section.new
  end

  test 'a blank section cannot be saved' do
    refute @section.save, 'Saved a blank section'
  end

  test 'course, free seats, employee, location, and schedule are all defined to save' do
    flunk
    @section.course = :test1
    @section.free_seats = 15
  end

  test 'course_id, employee_id, and location_id all refer to valid objects' do
    flunk
  end

  test 'only positive integers can be saved to free_seats' do
    flunk
  end

  test 'only current sections are denoted as current' do
    flunk
  end

  test 'the schedule must have a duration' do
    flunk
  end

  test 'schedules can be serialized and then rebuilt accurately from the database' do
    flunk
  end

  test 'class sections have to repeat weekly' do
    flunk
  end



end
