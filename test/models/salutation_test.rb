require 'test_helper'

class SalutationTest < ActiveSupport::TestCase

  setup do
    @salutation = Salutation.new
  end
  
  test 'name must be present and unique' do
    assert !@salutation.save
    @salutation.name = 'Mr.'
    assert @salutation.save
    @salutation.save
    @salutation      = Salutation.new
    @salutation.name = 'Mr.'
    assert !@salutation.save
  end

end
