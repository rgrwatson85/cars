require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  
  setup do
    @course            = Course.new
    @prerequisite      = Course.new
    @prerequisite_rule = CoursePrerequisite.new
  end

  
  test 'default values are set for new course' do
    assert_equal @course.name, Course::DEFAULT_NAME, 'Default course name not set'
    assert_equal @course.cost, Course::DEFAULT_COST, 'Default course cost not set'
    assert_equal @course.floating_date, Course::DEFAULT_FLOATING_DATE, 'Default floating date not set'
  end
  
  test 'course cannot have default name' do
    @course.cost          = 100
    @course.floating_date = false
    assert !@course.save
  end
  
  test 'course cannot have a cost less than zero' do
    @course.cost = -1
    assert !@course.save
  end
  
  test 'course cost must be numerical' do
    @course.cost = 'asdf'
    assert !@course.save
  end
  
  test 'a valid course can be saved' do
    @course.name          = 'Test Course Name'
    @course.cost          = 100
    @course.floating_date = false
    assert @course.save!
  end

  test 'course name must be unique' do
    @course.name          = 'Test Course Name'
    @course.cost          = 100
    @course.floating_date = false
    @course.save!
    @course               = Course.new
    @course.name          = 'Test Course Name'
    @course.cost          = 100
    @course.floating_date = false
    assert !@course.save
  end
  
  test 'prerequisites/required_by for new course returns an empty array' do
    assert @course.prerequisites.is_a?(Array), 'Not an array'
    assert_equal @course.has_prerequisites?, false, 'Array not empty'
    assert @course.required_by.is_a?(Array), 'Not an array - required_by'
    assert_equal @course.is_required?, false, 'Array not empty - required_by'
  end
  
  test 'a course\'s prerequisites/required_by are returned' do
    @course.name                        = 'Test Course Name'
    @course.cost                        = 100
    @course.floating_date               = false
    @prerequisite                       = Course.new
    @prerequisite.name                  = 'Test Prerequisite Name'
    @prerequisite.cost                  = 100
    @prerequisite.floating_date         = false
    @course.save!
    @prerequisite.save!
    @prerequisite_rule.course_id        = @course.id
    @prerequisite_rule.prerequisite_id  = @prerequisite.id
    @prerequisite_rule.allow_concurrent = true
    @prerequisite_rule.save!
    assert @course.has_prerequisites?, 'Course prerequisites not returned'
    assert @prerequisite.is_required?, 'Course not required'
    assert_equal @course.prerequisites[0], @prerequisite, 'Course prerequisite not valid'
    assert_equal @prerequisite.required_by[0], @course, 'Course required_by not valid'
  end
  
  test 'classes can be concurrently enrolled only if allowed' do
    @course.name                        = 'Test Course Name'
    @course.cost                        = 100
    @course.floating_date               = false
    @prerequisite                       = Course.new
    @prerequisite.name                  = 'Test Prerequisite Name'
    @prerequisite.cost                  = 100
    @prerequisite.floating_date         = false
    @course.save!
    @prerequisite.save!
    @prerequisite_rule.course_id        = @course.id
    @prerequisite_rule.prerequisite_id  = @prerequisite.id
    @prerequisite_rule.allow_concurrent = true
    @prerequisite_rule.save!
    assert @course.concurrent_enrollment_allowed? @prerequisite
    #change allow to false
    @prerequisite_rule.allow_concurrent = false
    @prerequisite_rule.save
    assert !(@course.concurrent_enrollment_allowed? @prerequisite)
  end
  
  test 'indirect prerequisite allow concurrent applies to lower level courses' do
    @course.name                         = 'Parent Course Name'
    @course.cost                         = 100
    @course.floating_date                = false
    @prerequisite                        = Course.new
    @prerequisite.name                   = 'First Prerequisite Name'
    @prerequisite.cost                   = 100
    @prerequisite.floating_date          = false
    @prerequisite2                       = Course.new()
    @prerequisite2.name                  = 'Second Prerequisite Name'
    @prerequisite2.cost                  = 100
    @prerequisite2.floating_date         = false
    @course.save!
    @prerequisite.save!
    @prerequisite2.save!
    @prerequisite_rule.course_id         = @course.id
    @prerequisite_rule.prerequisite_id   = @prerequisite.id
    @prerequisite_rule.allow_concurrent  = true
    @prerequisite_rule.save!
    @prerequisite2_rule                  = CoursePrerequisite.new()
    @prerequisite2_rule.course_id        = @prerequisite.id
    @prerequisite2_rule.prerequisite_id  = @prerequisite2.id
    @prerequisite2_rule.allow_concurrent = true
    @prerequisite2_rule.save!
    assert @course.concurrent_enrollment_allowed? @prerequisite2
    #change allow to false
    @prerequisite_rule.allow_concurrent  = false
    @prerequisite_rule.save!
    assert !(@course.concurrent_enrollment_allowed? @prerequisite2)
  end



end
