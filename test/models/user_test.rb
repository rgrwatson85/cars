require 'test_helper'

class UserTest < ActiveSupport::TestCase

  setup do
    @customer = Customer.new
    @employee = Employee.new
  end

  test 'is a customer of customer type' do
    assert @customer.is_customer?
  end

  test 'is a employee of employee type' do
    assert @employee.is_employee?
  end


end
