require 'test_helper'

class PaymentMethodTest < ActiveSupport::TestCase
  
  setup do
    @pm = PaymentMethod.new
  end
  
  test 'name must be present and unique' do
    assert !@pm.save
    @pm.name = 'Credit Card'
    assert @pm.save
    @pm.save
    @pm      = PaymentMethod.new
    @pm.name = 'Credit Card'
    assert !@pm.save
  end
  
end
