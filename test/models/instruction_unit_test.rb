require 'test_helper'

class InstructionUnitTest < ActiveSupport::TestCase
  
  setup do
    @iu = InstructionUnit.new
  end
  
  test 'must have name sequence and course to save' do
    @iu.name = 'test'
    assert !@iu.save
    @iu.sequence = 1
    assert !@iu.save
    @iu.course_id = -1
    assert !@iu.save
    @iu.course_id = 1
    assert @iu.save
  end
  
  test 'must have unique course sequence combination' do
    @iu.name = 'test'
    @iu.sequence = 1
    @iu.course_id = 1
    @iu.save
    @iu = InstructionUnit.new
    @iu.name = 'test'
    @iu.sequence = 1
    @iu.course_id = 1
    assert !@iu.save
  end
  
end
