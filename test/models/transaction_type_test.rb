require 'test_helper'

class TransactionTypeTest < ActiveSupport::TestCase

  setup do
    @tt = TransactionType.new
  end
  
  test 'name must be present and unique' do
    @tt.name = 'Enroll in course'
    assert !@tt.save
    @tt.due_in_days = 1
    assert @tt.save
    @tt.save
    @tt      = TransactionType.new
    @tt.name = 'Enroll in course'
    assert !@tt.save
  end

end
