require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  
  setup do
    @country = Country.new
  end
  
  test 'a country must be have a name to be saved' do
    assert !@country.save
    @country.name = 'USA'
    assert @country.save
  end
  
  test 'a country cannot have a duplicate name' do
    @country.name = 'USA'
    @country.save!
    @country = Country.new
    @country.name =  Country.first.name
    assert !@country.save
  end
  
end
