require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  
  setup do
    @trans = Transaction.new
    @trans.user = Customer.first
    @trans.transaction_type = TransactionType.first
    @trans.amount = 50
    @trans.date = Date.today
    @trans.description = 'Parent transaction'
    @trans.save!
  end
  
  test 'reponds to user' do
    assert @trans.respond_to? :user
  end
  
  test 'reponds to transaction type' do
    assert @trans.respond_to? :transaction_type
  end
  
  test 'reponds to payment method' do
    assert @trans.respond_to? :payment_method
  end
  
  test 'reponds to transaction' do
    assert @trans.respond_to? :transaction
  end
  
  test 'reponds to transactions' do
    assert @trans.respond_to? :transactions
  end

  test 'child transaction does not process payment' do
    @child = Transaction.new
    @child.user = Customer.first
    @child.transaction_type = TransactionType.first
    @child.amount = 50
    @child.date = Date.today
    @child.description = 'Parent transaction'
    @child.payment_method = PaymentMethod.first
    @child.transaction = @trans
    @child.save!

    assert !@child.process_payment(50, @child.payment_method)
  end

  test 'transaction with zero balance does not process payment' do
    @trans.process_payment(@trans.amount, PaymentMethod.first)
    assert !@trans.process_payment(10, PaymentMethod.first)
  end
  
end
