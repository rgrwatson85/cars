require 'test_helper'

class CourseProgressesControllerTest < ActionController::TestCase
  setup do
    @course_progress = course_progresses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:course_progresses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create course_progress" do
    assert_difference('CourseProgress.count') do
      post :create, course_progress: {  }
    end

    assert_redirected_to course_progress_path(assigns(:course_progress))
  end

  test "should show course_progress" do
    get :show, id: @course_progress
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @course_progress
    assert_response :success
  end

  test "should update course_progress" do
    patch :update, id: @course_progress, course_progress: {  }
    assert_redirected_to course_progress_path(assigns(:course_progress))
  end

  test "should destroy course_progress" do
    assert_difference('CourseProgress.count', -1) do
      delete :destroy, id: @course_progress
    end

    assert_redirected_to course_progresses_path
  end
end
