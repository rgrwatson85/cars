require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, user: { address2: @user.address2, address: @user.address, cell_phone: @user.cell_phone, city: @user.city, dob: @user.dob, email_address: @user.email_address, first_name: @user.first_name, home_phone: @user.home_phone, last_name: @user.last_name, last_name_suffix_id: @user.last_name_suffix_id, postal_code: @user.postal_code, salutation_id: @user.salutation_id, state_id: @user.state_id }
    end

    assert_redirected_to user_path(assigns(:user))
  end

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user
    assert_response :success
  end

  test "should update user" do
    patch :update, id: @user, user: { address2: @user.address2, address: @user.address, cell_phone: @user.cell_phone, city: @user.city, dob: @user.dob, email_address: @user.email_address, first_name: @user.first_name, home_phone: @user.home_phone, last_name: @user.last_name, last_name_suffix_id: @user.last_name_suffix_id, postal_code: @user.postal_code, salutation_id: @user.salutation_id, state_id: @user.state_id }
    assert_redirected_to user_path(assigns(:user))
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, id: @user
    end

    assert_redirected_to users_path
  end
end
