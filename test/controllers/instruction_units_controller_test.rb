require 'test_helper'

class InstructionUnitsControllerTest < ActionController::TestCase
  setup do
    @instruction_unit = instruction_units(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:instruction_units)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create instruction_unit" do
    assert_difference('InstructionUnit.count') do
      post :create, instruction_unit: {  }
    end

    assert_redirected_to instruction_unit_path(assigns(:instruction_unit))
  end

  test "should show instruction_unit" do
    get :show, id: @instruction_unit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @instruction_unit
    assert_response :success
  end

  test "should update instruction_unit" do
    patch :update, id: @instruction_unit, instruction_unit: {  }
    assert_redirected_to instruction_unit_path(assigns(:instruction_unit))
  end

  test "should destroy instruction_unit" do
    assert_difference('InstructionUnit.count', -1) do
      delete :destroy, id: @instruction_unit
    end

    assert_redirected_to instruction_units_path
  end
end
