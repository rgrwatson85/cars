# Ordered lookup tables
load "#{Rails.root}/db/seeds_data/countries.rb"
load "#{Rails.root}/db/seeds_data/states.rb"

# Unordered lookup tables
load "#{Rails.root}/db/seeds_data/attendance_types.rb"
load "#{Rails.root}/db/seeds_data/courses.rb"
load "#{Rails.root}/db/seeds_data/course_prerequisites.rb"
load "#{Rails.root}/db/seeds_data/instruction_units.rb"
load "#{Rails.root}/db/seeds_data/last_name_suffixes.rb"
load "#{Rails.root}/db/seeds_data/locations.rb"
load "#{Rails.root}/db/seeds_data/payment_methods.rb"
load "#{Rails.root}/db/seeds_data/salutations.rb"
load "#{Rails.root}/db/seeds_data/transaction_types.rb"

# Ordered core tables
load "#{Rails.root}/db/seeds_data/users.rb"
load "#{Rails.root}/db/seeds_data/sections.rb"

# No sample data in these tables yet
load "#{Rails.root}/db/seeds_data/transactions.rb"
load "#{Rails.root}/db/seeds_data/enrollments.rb"
load "#{Rails.root}/db/seeds_data/course_progresses.rb"
load "#{Rails.root}/db/seeds_data/attendances.rb"




