class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.references :course, index: true
      t.date :start_date
      t.date :end_date
      t.time :start_time
      t.time :end_time
      t.integer :free_seats
      t.integer :recurrence
      t.references :employee, index: true
      t.references :location, index: true

      t.timestamps
    end
  end
end
