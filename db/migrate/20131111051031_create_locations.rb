class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :address
      t.string :address2
      t.string :city
      t.references :state, index: true
      t.string :postal_code

      t.timestamps
    end
  end
end
