class AddDefaultValueToType < ActiveRecord::Migration
  def change
    # User type for authorization. Default value of Guest.
    change_column :users, :type, :string, :null => false, :default => 'Guest'
  end
end
