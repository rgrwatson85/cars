class AddUserTypeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_type, :string
    add_index :users, [:user_type], name: :index_users_on_user_type
  end
end
