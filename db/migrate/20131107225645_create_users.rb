class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.references :last_name_suffix, index: true
      t.string :home_phone
      t.string :cell_phone
      t.string :address
      t.string :address2
      t.string :city
      t.references :state, index: true
      t.string :postal_code
      t.string :email_address
      t.date   :dob
      t.references :salutation, index: true

      t.timestamps
    end
  end
end
