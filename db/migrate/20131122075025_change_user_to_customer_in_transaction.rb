class ChangeUserToCustomerInTransaction < ActiveRecord::Migration
  def change
    revert do
      add_reference :transactions, :user, index: true
    end
    add_reference :transactions, :customer, index: true
  end
end
