class CreateInstructionUnits < ActiveRecord::Migration
  def change
    create_table :instruction_units do |t|
      t.string :name
      t.integer :sequence
      t.references :course, index: true

      t.timestamps
    end
  end
end
