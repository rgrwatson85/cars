class CreateCoursePrerequisites < ActiveRecord::Migration
  def change
    create_table :course_prerequisites do |t|
      t.integer :course_id
      t.integer :prerequisite_id
      t.boolean :allow_concurrent

      t.timestamps
    end
  end
end
