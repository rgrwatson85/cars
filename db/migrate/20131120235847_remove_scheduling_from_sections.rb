class RemoveSchedulingFromSections < ActiveRecord::Migration
  def change
    remove_column :sections, :start_date, :string
    remove_column :sections, :end_date, :string
    remove_column :sections, :start_time, :string
    remove_column :sections, :end_time, :string
    remove_column :sections, :recurrence, :string
  end
end
