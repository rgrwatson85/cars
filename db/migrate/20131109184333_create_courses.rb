class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.integer :cost
      t.boolean :floating_date

      t.timestamps
    end
  end
end
