class CreateLastNameSuffixes < ActiveRecord::Migration
  def change
    create_table :last_name_suffixes do |t|
      t.string :name

      t.timestamps
    end
  end
end
