class AddTransactionIdToEnrollment < ActiveRecord::Migration
  def change
    add_reference :enrollments, :transaction, index: true
  end
end
