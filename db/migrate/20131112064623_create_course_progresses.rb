class CreateCourseProgresses < ActiveRecord::Migration
  def change
    create_table :course_progresses do |t|
      t.references :customer, index: true
      t.references :instruction_unit, index: true
      t.references :section, index: true

      t.timestamps
    end
  end
end
