class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.references :attendance_type, index: true
      t.references :enrollment, index: true
      t.date :record_date

      t.timestamps
    end
  end
end
