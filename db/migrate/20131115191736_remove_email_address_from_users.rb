class RemoveEmailAddressFromUsers < ActiveRecord::Migration

  #No longer need the email_address column as devise added it's own
  def change
    remove_column :users, :email_address
  end
end
