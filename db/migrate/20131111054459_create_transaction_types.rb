class CreateTransactionTypes < ActiveRecord::Migration
  def change
    create_table :transaction_types do |t|
      t.string :name
      t.integer :due_in_days

      t.timestamps
    end
  end
end
