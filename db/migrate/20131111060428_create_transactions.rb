class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :user, index: true
      t.references :transaction_type, index: true
      t.integer :amount
      t.datetime :date
      t.text :description
      t.references :payment_method, index: true
      t.references :transaction, index: true

      t.timestamps
    end
  end
end
