class AddScheduleToSections < ActiveRecord::Migration
  def change
    add_column :sections, :schedule, :text
  end
end
