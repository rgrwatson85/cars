AttendanceType.create!([
  {:name => 'Present'},
  {:name => 'Late For Class'},
  {:name => 'Absent - Unexcused'},
  {:name => 'Absent - Excused'}
])