include IceCube
last_month  = Time.now.midnight - 8.weeks
this_month  = Time.now.midnight - 1.week
next_month  = Time.now.midnight + 9.weeks
  
klancaster  = Employee.where(first_name: 'Keith', last_name: 'Lancaster').take!
ahensley    = Employee.where(first_name: 'Alan', last_name: 'Hensley').take!
  
houston     = Location.find_by_city('Houston')
san_antonio = Location.find_by_city('San Antonio')
#
# Teen Driving Lecture sections
#
teen_driving_lecture = Course.find_by_name('Teen Driving - Lecture')
Section.create!([
    {   # Teen Class last month @ 3 in Houston, MWF
        course:     teen_driving_lecture,
        employee:   klancaster,
        location:   houston,
        free_seats: 2,
        schedule:   Schedule.new(last_month + 15.hours, duration: 90.minutes),
        recurs_on:  [:monday, :wednesday, :friday]
    },
    {   # Teen Class last month @ 5 in Houston, MWF
        course:     teen_driving_lecture,
        employee:   ahensley,
        location:   houston,
        free_seats: 4,
        schedule:   Schedule.new(last_month + 17.hours, duration: 90.minutes),
        recurs_on:  [:monday, :wednesday, :friday]
    },
    {   # Teen Class just started @ 3 in Houston, MWF
        course:     teen_driving_lecture,
        employee:   klancaster,
        location:   houston,
        free_seats: 4,
        schedule:   Schedule.new(this_month + 15.hours, duration: 90.minutes),
        recurs_on:  [:monday, :wednesday, :friday]
    },
    {   # Teen Class just started @ 5 in Houston, MWF
        course:     teen_driving_lecture,
        employee:   ahensley,
        location:   houston,
        free_seats: 6,
        schedule:   Schedule.new(this_month + 17.hours, duration: 90.minutes),
        recurs_on:  [:monday, :wednesday, :friday]
    },
    {   # Teen Class next month @ 3 in Houston, MWF
        course:     teen_driving_lecture,
        employee:   klancaster,
        location:   houston,
        free_seats: 21,
        schedule:   Schedule.new(next_month + 15.hours, duration: 90.minutes),
        recurs_on:  [:monday, :wednesday, :friday]
    },
    {   # Teen Class next month @ 5 in Houston, MWF
        course:     teen_driving_lecture,
        employee:   ahensley,
        location:   houston,
        free_seats: 25,
        schedule:   Schedule.new(next_month + 17.hours, duration: 90.minutes),
        recurs_on:  [:monday, :wednesday, :friday]
    }
])
#
# Teen Driving Road sections
#

Section.create({
    course:     Course.find_by_name('Teen Driving - Road'),
    employee:   Employee.where(first_name: 'B.A.', last_name: 'Baracus').take!,
    location:   houston,
    free_seats: 2,
    schedule:   Schedule.new(Time.now.midnight - 6.weeks + 14.hours, duration: 60.minutes),
    recurs_on:  [:monday, :tuesday, :wednesday, :thursday, :friday]
               })

#
# Adult Driving Lecture sections
#
adult_driving_lecture = Course.find_by_name('Adult Driving - Lecture')
Section.create!([
    {   # Adult Class last month @ 10 in Houston, TTh
        course:     adult_driving_lecture,
        employee:   klancaster,
        location:   houston,
        free_seats: 3,
        schedule:   Schedule.new(last_month + 10.hours, duration: 90.minutes),
        recurs_on:  [:tuesday, :thursday]
    },
    {   # Adult Class last month @ 6 in Houston, TTh
        course:     adult_driving_lecture,
        employee:   ahensley,
        location:   houston,
        free_seats: 0,
        schedule:   Schedule.new(last_month + 18.hours, duration: 90.minutes),
        recurs_on:  [:tuesday, :thursday]
    },
    {   # Adult Class just started @ 10 in Houston, TTh
        course:     adult_driving_lecture,
        employee:   klancaster,
        location:   houston,
        free_seats: 0,
        schedule:   Schedule.new(this_month + 10.hours, duration: 90.minutes),
        recurs_on:  [:tuesday, :thursday]
    },
    {   # Adult Class just started @ 6 in Houston, TTh
        course:     adult_driving_lecture,
        employee:   ahensley,
        location:   houston,
        free_seats: 2,
        schedule:   Schedule.new(this_month + 18.hours, duration: 90.minutes),
        recurs_on:  [:tuesday, :thursday]
    },
    {   # Adult Class next month @ 10 in Houston, TTh
        course:     adult_driving_lecture,
        employee:   klancaster,
        location:   houston,
        free_seats: 8,
        schedule:   Schedule.new(next_month + 10.hours, duration: 90.minutes),
        recurs_on:  [:tuesday, :thursday]
    },
    {   # Adult Class next month @ 6 in Houston, TTh
        course:     adult_driving_lecture,
        employee:   ahensley,
        location:   houston,
        free_seats: 13,
        schedule:   Schedule.new(next_month + 18.hours, duration: 90.minutes),
        recurs_on:  [:tuesday, :thursday]
    }
                ])

#
# Adult Driving Road sections
#

Section.create({
    course:     Course.find_by_name('Adult Driving - Road'),
    employee:   Employee.where(first_name: 'B.A.', last_name: 'Baracus').take!,
    location:   houston,
    free_seats: 2,
    schedule:   Schedule.new(Time.now.midnight - 6.weeks + 16.hours, duration: 60.minutes),
    recurs_on:  [:monday, :tuesday, :wednesday, :thursday, :friday]
               })

#
# Defensive Driving Sections
#
[last_month - 2.weeks, this_month, next_month + 2.weeks].each do |start|
  Section.create!([{  # Defensive Driving, Houston, Sat mornings
                      course:     Course.find_by_name('Defensive Driving'),
                      employee:   klancaster,
                      location:   houston,
                      free_seats: (0..20).to_a.sample,
                      schedule:   Schedule.new(start + 9.hours, duration: 2.hours),
                      recurs_on:  [:saturday]
                  },
                  {   # Defensive Driving, Houston, Sat evenings
                      course:     Course.find_by_name('Defensive Driving'),
                      employee:   Employee.where(first_name: 'Sally', last_name: 'Ride').take!,
                      location:   houston,
                      free_seats: (0..20).to_a.sample,
                      schedule:   Schedule.new(start + 18.5.hours, duration: 2.hours),
                      recurs_on:  [:saturday]
                  }])
end

#
# Offensive Driving section
#
Section.create!({
    course:     Course.find_by_name('Advanced Offensive Driving'),
    employee:   Employee.where(first_name: 'Bob', last_name: 'Barker').take!,
    location:   san_antonio,
    free_seats: 2,
    schedule:   Schedule.new(Time.now.midnight + 3.weeks + 2.hours, duration: 5.hours),
    recurs_on:  [:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday]
               })
