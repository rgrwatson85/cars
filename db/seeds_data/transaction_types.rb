TransactionType.create!([
  {:name => 'Enroll In Course',:due_in_days => 21},
  {:name => 'Unenroll From Course',:due_in_days => 0},
  {:name => 'Transfer Course',:due_in_days => 0},
  {:name => 'Assess Penalty Fee',:due_in_days => 30},
  {:name => 'Payment Towards Balance',:due_in_days => 0}
])