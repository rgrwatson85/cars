PaymentMethod.create!([
  {:name => 'Credit Card'},
  {:name => 'Check'},
  {:name => 'Cash'},
  {:name => 'Barter'},
  {:name => 'Interstellar Credits'}
])
