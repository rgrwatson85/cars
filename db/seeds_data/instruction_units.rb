## Partially sourced from Tiger Driving School's Syllabus
## http://www.tigerdrivingschool.com/fees-a-requirements/6

standard_class_ius = ['Signs, Signals',
                     'Road Markings, Rules of the Road',
                     'Sharing the Road',
                     'Driving Maneuvers',
                     'Motor Vehicle Operation',
                     'Insurance Responsibilities',
                     'Seat Belt Awareness',
                     'Alcohol\'s Effects on Drivers',
                     'Motor Vehicle Maintenance',
                     'Exit Test'
]
standard_wheel_ius = ['Starting',
                     'Backing',
                     'Parallel Parking',
                     'Hill Parking',
                     'Turns',
                     'Handling Intersections',
                     'Lane Following and Changing',
                     'Pedestrian Considerations',
                     'Signalling',
                     'Braking',
                     'Advanced Braking'
]
teen_driving_class = Course.find_by_name('Teen Driving - Lecture')
teen_driving_wheel = Course.find_by_name('Teen Driving - Road')
adult_driving_class = Course.find_by_name('Adult Driving - Lecture')
adult_driving_wheel = Course.find_by_name('Adult Driving - Road')

standard_class_ius.each_with_index do |iu, i|
  InstructionUnit.create!(name:iu, sequence:i, course:teen_driving_class)
  InstructionUnit.create!(name:iu, sequence:i, course:adult_driving_class)
end

standard_wheel_ius.each_with_index do |iu, i|
  InstructionUnit.create!(name:iu, sequence:i, course:teen_driving_wheel)
  InstructionUnit.create!(name:iu, sequence:i, course:adult_driving_wheel)
end

def_driving_class = Course.find_by_name('Defensive Driving')
def_driving_ius = ['Traffic Safety',
                  'Human Performance',
                  'Rules of the Road',
                  'Situational Awareness',
                  'Dangerous Driving Scenarios',
                  'Driving Defensively',
                  'Alcohol and Driving',
                  'Exit Test'
]
def_driving_ius.each_with_index do |iu, i|
  InstructionUnit.create!(name:iu, sequence:i, course:def_driving_class)
end

off_driving_class = Course.find_by_name('Advanced Offensive Driving')
off_driving_ius = ['Protecting What\'s Yours',
                   'Lane Dominance',
                   'Effective Highway Communication',
                   'Weaving Your Way to Work',
                   'Traffic Signal Timing',
                   'Just How Invincible Are You?',
                   'Controlling Merge Flow',
                   'Your Rights as a Driver'
]
off_driving_ius.each_with_index do |iu, i|
  InstructionUnit.create!(name:iu, sequence:i, course:off_driving_class)
end
