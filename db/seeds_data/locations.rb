Location.create!({
    address:     '6625 Fake Street',
    address2:    'P.O. Box 217M',
    city:        'Houston',
    state:       State.find_by_abbr('TX'),
    postal_code: '77014'
                 })

Location.create!({
    address:      '8762 Rogerdale Lane',
    city:         'San Antonio',
    state:        State.find_by_abbr('TX'),
    postal_code:  '78023'
                 })
