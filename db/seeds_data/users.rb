Customer.create!([
{:first_name => 'Morgan',
:last_name => 'Watson',
:last_name_suffix_id => nil,
:home_phone => '702-629-0114',
:cell_phone => '702-629-0114',
:address => '1234 Fake St',
:address2 => nil,
:city => 'Smallville',
:state => State.find_by_abbr('TX'),
:postal_code => '77034',
:email => 'mail@email.com',
:password => 'password',
:dob => Date.new(1985, 1, 23),
:salutation => Salutation.find_by_name('Mr.')}
])

Customer.create!([
   {:first_name => 'New',
    :last_name => 'User',
    :last_name_suffix_id => nil,
    :home_phone => '702-629-0114',
    :cell_phone => '702-629-0114',
    :address => '1234 Fake St',
    :address2 => nil,
    :city => 'Smallville',
    :state => State.find_by_abbr('TX'),
    :postal_code => '77034',
    :email => 'mail3@email.com',
    :password => 'password',
    :dob => Date.new(1985, 1, 23),
    :salutation => Salutation.find_by_name('Mr.')}
])

User.create!([
{:first_name => 'Keith',
:last_name => 'Lancaster',
:last_name_suffix_id => nil,
:home_phone => '713-245-6546',
:cell_phone => '713-543-2310',
:address => '1239 Fake St',
:address2 => nil,
:city => 'Smallville',
:state => State.find_by_abbr('TX'),
:postal_code => '77043',
:email => 'mail2@email.com',
:password => 'password',
:dob => Date.new(1955, 12, 25),
:salutation => Salutation.find_by_name('Mr.'),
:type => "Employee"}
])

Employee.create!({
    first_name:       'Alan',
    last_name:        'Hensley',
    last_name_suffix: LastNameSuffix.find_by_name('VI'),
    home_phone:       '281-395-3858',
    cell_phone:       '713-882-8592',
    address:          '3953 Real St',
    city:             'Houston',
    state:            State.find_by_abbr('TX'),
    postal_code:      '77042',
    email:            'alan@alan.com',
    password:         'alanpass',
    dob:              Date.new(1989, 3, 28),
    salutation:       Salutation.find_by_name('Hon.')
                 })

Employee.create!({
    first_name:       'Bob',
    last_name:        'Barker',
    home_phone:       '875-332-3345',
    cell_phone:       '875-234-7899',
    address:          '4444 Right Price Road',
    city:             'New Braunfels',
    state:            State.find_by_abbr('TX'),
    postal_code:      '78155',
    email:            'bob@bob.com',
    password:         'rightprice',
    dob:              Date.new(1923, 12, 12),
    salutation:       Salutation.find_by_name('Mr.')
                 })

Employee.create!({
    first_name:       'Sally',
    last_name:        'Ride',
    home_phone:       '345-644-3345',
    cell_phone:       '713-388-0092',
    address:          '41 Endeavor Circle',
    city:             'Low Earth Orbit',
    state:            State.find_by_abbr('TX'),
    postal_code:      '00000',
    email:            'sally@nasa.gov',
    password:         '321liftoff',
    dob:              Date.new(1951, 5, 26),
    salutation:       Salutation.find_by_name('Ms.')
                 })

Employee.create!({
    first_name:       'B.A.',
    last_name:        'Baracus',
    home_phone:       '345-345-3345',
    cell_phone:       '713-323-2932',
    address:          '17 Pity Lane',
    city:             'Houston',
    state:            State.find_by_abbr('TX'),
    postal_code:      '77264',
    email:            'ba@vanthusiast.org',
    password:         'pitydafool',
    dob:              Date.new(1976, 3, 18),
    salutation:       Salutation.find_by_name('Mr.')
                 })
