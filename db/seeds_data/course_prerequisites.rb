# Teen Driving - Lecture must be completed before or concurrently with phase 2
CoursePrerequisite.create!(course_id:Course.find_by_name('Teen Driving - Road').id,
                           prerequisite_id:Course.find_by_name('Teen Driving - Lecture').id,
                           allow_concurrent:true)

# Adult Driving - Lecture must be completed before or concurrently with phase 2
CoursePrerequisite.create!(course_id:Course.find_by_name('Adult Driving - Road').id,
                           prerequisite_id:Course.find_by_name('Adult Driving - Lecture').id,
                           allow_concurrent:true)

# Defensive Driving must be completed before Advanced Offensive Driving
CoursePrerequisite.create!(course_id:Course.find_by_name('Advanced Offensive Driving').id,
                           prerequisite_id:Course.find_by_name('Defensive Driving').id,
                           allow_concurrent:false)
