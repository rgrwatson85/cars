# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131122075025) do

  create_table "attendance_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "attendances", force: true do |t|
    t.integer  "attendance_type_id"
    t.integer  "enrollment_id"
    t.date     "record_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attendances", ["attendance_type_id"], name: "index_attendances_on_attendance_type_id", using: :btree
  add_index "attendances", ["enrollment_id"], name: "index_attendances_on_enrollment_id", using: :btree

  create_table "countries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_prerequisites", force: true do |t|
    t.integer  "course_id"
    t.integer  "prerequisite_id"
    t.boolean  "allow_concurrent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "course_progresses", force: true do |t|
    t.integer  "customer_id"
    t.integer  "instruction_unit_id"
    t.integer  "section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "course_progresses", ["customer_id"], name: "index_course_progresses_on_customer_id", using: :btree
  add_index "course_progresses", ["instruction_unit_id"], name: "index_course_progresses_on_instruction_unit_id", using: :btree
  add_index "course_progresses", ["section_id"], name: "index_course_progresses_on_section_id", using: :btree

  create_table "courses", force: true do |t|
    t.string   "name"
    t.integer  "cost"
    t.boolean  "floating_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "enrollments", force: true do |t|
    t.integer  "customer_id"
    t.integer  "section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "transaction_id"
  end

  add_index "enrollments", ["customer_id"], name: "index_enrollments_on_customer_id", using: :btree
  add_index "enrollments", ["section_id"], name: "index_enrollments_on_section_id", using: :btree
  add_index "enrollments", ["transaction_id"], name: "index_enrollments_on_transaction_id", using: :btree

  create_table "instruction_units", force: true do |t|
    t.string   "name"
    t.integer  "sequence"
    t.integer  "course_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "instruction_units", ["course_id"], name: "index_instruction_units_on_course_id", using: :btree

  create_table "last_name_suffixes", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.string   "address"
    t.string   "address2"
    t.string   "city"
    t.integer  "state_id"
    t.string   "postal_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["state_id"], name: "index_locations_on_state_id", using: :btree

  create_table "payment_methods", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salutations", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sections", force: true do |t|
    t.integer  "course_id"
    t.integer  "free_seats"
    t.integer  "employee_id"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "schedule"
  end

  add_index "sections", ["course_id"], name: "index_sections_on_course_id", using: :btree
  add_index "sections", ["employee_id"], name: "index_sections_on_employee_id", using: :btree
  add_index "sections", ["location_id"], name: "index_sections_on_location_id", using: :btree

  create_table "states", force: true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "abbr"
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree

  create_table "transaction_types", force: true do |t|
    t.string   "name"
    t.integer  "due_in_days"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transactions", force: true do |t|
    t.integer  "transaction_type_id"
    t.integer  "amount"
    t.datetime "date"
    t.text     "description"
    t.integer  "payment_method_id"
    t.integer  "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "customer_id"
  end

  add_index "transactions", ["customer_id"], name: "index_transactions_on_customer_id", using: :btree
  add_index "transactions", ["payment_method_id"], name: "index_transactions_on_payment_method_id", using: :btree
  add_index "transactions", ["transaction_id"], name: "index_transactions_on_transaction_id", using: :btree
  add_index "transactions", ["transaction_type_id"], name: "index_transactions_on_transaction_type_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "last_name_suffix_id"
    t.string   "home_phone"
    t.string   "cell_phone"
    t.string   "address"
    t.string   "address2"
    t.string   "city"
    t.integer  "state_id"
    t.string   "postal_code"
    t.date     "dob"
    t.integer  "salutation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type",                   default: "Customer", null: false
    t.string   "email",                  default: "",         null: false
    t.string   "encrypted_password",     default: "",         null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["last_name_suffix_id"], name: "index_users_on_last_name_suffix_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["salutation_id"], name: "index_users_on_salutation_id", using: :btree
  add_index "users", ["state_id"], name: "index_users_on_state_id", using: :btree
  add_index "users", ["type"], name: "index_users_on_type", using: :btree

end
